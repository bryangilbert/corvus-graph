import * as d3 from 'd3'

export const userGraphHelper = function (d3) {
  return {
    buildGraph,
    createSVG,
    createCrossHair
  }
}

export const getDims = function getDims() {
  let dims = {
    top: 30, // top margin just aesthetic
    right: 10, //right margin -- wierd this needs to be 150 why?
    bottom: 50, // bottom margin leaves space for floating date text along axis
    left: 5, // left margin
    yAxisOffset: -70 //space per y  axis
  }
  dims.height = window.innerHeight * 0.70 - dims.top - dims.bottom
  dims.width = window.innerWidth * 0.70 - dims.left - dims.right
  return dims
}


/**
 * Creates and returns a svg group for drawing the graph
 * @param graphElementId
 * @param dims
 * @return {*}
 */
function createSVG (graphElementId, system) {
  let dims = system.dims
  let left = dims.left - system.cumulativeOffset
  const id = '#' + graphElementId

  let width = window.innerWidth * 0.85
  let height = window.innerHeight * 0.70

  let viewWidth = width
  let viewHeight = height
  let viewX = system.cumulativeOffset
  let viewY = - dims.top

  d3.select(id).selectAll('svg').remove()
  let svg = d3.select(id)
    .append('svg')
    // Responsive SVG needs these 2 attributes and no width and height attr.
    .attr('preserveAspectRatio', 'xMinYMin meet')
    .attr('viewBox', `${viewX}, ${viewY}, ${viewWidth}, ${viewHeight}`)

  svg.append('rect')
    .attr('height', viewHeight)
    .attr('width', viewWidth)
    .style('fill', 'white')
    .style('stroke', 'black')
    .style('stroke-width', 2)
    .attr('transform', `translate(${viewX}, ${viewY})`)

  return svg
}

/**
 * Creates a crosshairs that follow the mouse.
 * Includes active labels on each axis.
 * @param svg
 * @param dims
 * @param series
 */
function createCrossHair(svg, system) {
  const dims = system.dims
  let crosshairGroup = svg.append('g')
  let axisTexts = system.allSeries.map( (s) => {
    return s.axisText
  })

  // Create the circle that travels along the curve of chart
  var focusCircle = crosshairGroup
    .append('circle')
    .style('fill', 'none')
    .attr('stroke', 'black')
    .attr('r', 8.5)
    .style('opacity', 0)

  function makeCrossLine (left, right, top, bottom) {
    return crosshairGroup
      .append('line')
      .style('fill', 'none')
      .attr('stroke', 'black')
      .attr('stroke-width', '1')
      .style('stroke-dasharray', [1,4])
      .attr('r', 8.5)
      .attr('x1',left)
      .attr('x2',right)
      .attr('y1',top)
      .attr('y2',bottom)
      .style('opacity', 0)
  }
  var crossX = makeCrossLine(0,0,0,dims.height)
  var crossY = makeCrossLine(-100, dims.width,0,0)

  // create rect that covers area to capture all mouse events
  svg
    .append('rect')
    .style('fill', 'none')
    .style('pointer-events', 'all')
    .attr('width', dims.width)
    .attr('height', dims.height)
    .on('mouseover', () => showHide(1))
    .on('mousemove', mousemove)
    .on('mouseout', () => showHide(0))

  function showHide (opacity) {
    crosshairGroup.style('opacity', opacity)
    focusCircle.style('opacity', opacity)
    crossX.style('opacity', opacity)
    crossY.style('opacity', opacity)
    system.allSeries.forEach( (group) => {
      let yText = group.yAxisTextGroup.selectAll('text')
      // set the text and move the text to match the cursor
      yText.style('opacity', opacity)
      let yTextRect = group.yAxisTextGroup.selectAll('rect')
      yTextRect.style('opacity', opacity)
      let xText = group.xAxisTextGroup.selectAll('text')
      xText.style('opacity', opacity)
    })

  }

  function mousemove (event) {
    // recover coordinate we need
    let ptn = d3.pointer(event)
    let x = ptn[0]
    let y = ptn[1]
    focusCircle
      .attr('cx', x)
      .attr('cy', y)
    crossX
      .attr('x1', x)
      .attr('x2', x)
    crossY
      .attr('y1', y)
      .attr('y2', y)
    // for each y axis update the movable text
    system.allSeries.forEach( (group) => {
      let v = `${Math.round(group.yScale.invert(y))} ${group.type.units}`
      let d = d3.timeFormat('%Y-%m-%d %H:%M:%S %Z')(group.xScale.invert(x))
      // find the text element inside the group created for a moving text box
      let yText = group.yAxisTextGroup.selectAll('text')
      // set the text and move the text to match the cursor
      yText.attr('y',y).html(v)
      // adjust position of rect that comes with y text
      let yTextRect = group.yAxisTextGroup.selectAll('rect')
      yTextRect.attr('y',y - 15)
      // set the date value into the x axis text
      let xText = group.xAxisTextGroup.selectAll('text')
      xText.attr('x',x).html(d)
    })
  }
} // end createCrossHair

/**
 * Main function that builds the graph with the given data
 * @param svg
 * @param dims
 * @param allSeries
 */
function buildGraph (svg, system) {
  const dims = system.dims
  system.allSeries.forEach( someSeries => {
    addXaxis(svg,someSeries, dims)
    addYaxis(svg,someSeries, dims)
    // Add the linef active
    // console.log('buildGraph',someSeries)
    someSeries.series.forEach( dataSeries => {
      addLine(svg, someSeries, dataSeries)
      // console.log('buildGraph', dataSeries)
    })
  })
}

// ----------- buildGraph helpers --------------

function addLine(svg, someSeries, dataSeries) {
  let lineColour = someSeries.lineColour
  const factor = (dataSeries.index*3 % 10)/10
  lineColour = d3.interpolateHslLong(lineColour, 'lightgrey')(factor)
  dataSeries.lineColour = lineColour
  // console.log('clr', dataSeries.index, dataSeries.name, lineColour)

  const dataLine = d3.line()
    .x(function (d) { return someSeries.xScale(d.date) })
    .y(function (d) { return someSeries.yScale(d.value) })
    .defined(function(d) { return d.date && d.value })
  svg.append('path')
    // .datum(dataSeries.data)
    .attr('fill', 'none')
    .attr('stroke', lineColour)
    .attr('stroke-width', 1.5)
    .attr('d', dataLine(dataSeries.data))
}

function addXaxis(svg, someSeries, dims) {
  // Add X axis --> it is a date format
  const xExtent = [someSeries.minDate, someSeries.maxDate]
  someSeries.xScale = d3.scaleTime()
    .domain(xExtent)
    .range([0, dims.width])
  let xAxis = svg.append('g')
    .classed('xAxis', true)
    .attr('transform', 'translate(0,' + dims.height + ')')
    .call(d3.axisBottom(someSeries.xScale))
  let xAxisTextGroup = xAxis.append('g') // create a group so the moving text can be wrapped in a rect
  xAxisTextGroup.append('text')
    .classed('axisText', true)
    .attr('y', 30)
  someSeries.xAxisTextGroup = xAxisTextGroup
}

function addYaxis(svg, someSeries, dims) {
  // console.log('add y axis', someSeries)
  const lineColour = someSeries.type.lineColour
  const units = someSeries.type.units
  const yAxisOffset = someSeries.index * dims.yAxisOffset
  // Add Y axis
  const yExtent = [someSeries.minValue - 20, someSeries.maxValue + 20]
  someSeries.yScale = d3.scaleLinear()
    .domain(yExtent)
    .range([dims.height, 0])
  let yAxis = svg.append('g')
    .classed('yAxis', true)
    .attr('stroke', lineColour)
    .attr('transform', 'translate(' + yAxisOffset + ',0)')
    .call(d3.axisLeft(someSeries.yScale).tickFormat(x => `${x} ${units}`))
    /*  // uncomment to add a label to the y axis
  yAxis.append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", -60)
    .attr("x",0 - (dims.height / 2))
    .attr("dy", "1em")
    .style("text-anchor", "middle")
    .text("Value");
   */
  // create a group so the moving text can be wrapped in a rect
  let yAxisTextGroup = yAxis.append('g')
  yAxisTextGroup
    .append('rect')
    .style('fill', 'white')
    .style('pointer-events', 'none')
    .attr('x', -55) // magic num to shift left so the text is nicely placed
    .attr('width', '5em')
    .attr('height', '2em')
    .style('opacity', 0) // hide until needed

  yAxisTextGroup.append('text')
    .classed('axisText', true)
    .attr('x', -10)
    .style('opacity', 0) // hide until needed


  someSeries.yAxisTextGroup = yAxisTextGroup
}
