
/*
Import font awesome icons

https://fontawesome.com/v4.7.0/icons/

*/
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faAngleLeft,
  faAngleRight,
  faArrowLeft,
  faArrowRight,
  faBookOpen,
  faChevronDown,
  faChevronUp,
  faCheckCircle,
  faCircle,
  faClock,
  faDiceD20,
  faDownload,
  faEdit,
  faFilePdf,
  faHammer,
  faList,
  faListAlt,
  faChartLine,
  faShareAlt,
  faThList,
  faTimes,
  faTimesCircle,
  faTrash,
  faUpload,
  faBars
} from '@fortawesome/free-solid-svg-icons'
import { faFontAwesome, faCanadianMapleLeaf } from '@fortawesome/free-brands-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

export default function (Vue) {
  library.add(
    faAngleLeft,
    faAngleRight,
    faArrowLeft,
    faArrowRight,
    faBookOpen,
    faChevronDown,
    faChevronUp,
    faCheckCircle,
    faCircle,
    faClock,
    faDiceD20,
    faDownload,
    faEdit,
    faFilePdf,
    faChartLine,
    faHammer,
    faList,
    faListAlt,
    faShareAlt,
    faThList,
    faTimes,
    faTimesCircle,
    faTrash,
    faUpload,
    faBars
  )
  library.add(
    faFontAwesome,
    faCanadianMapleLeaf
  )
  // IN CODE sample usage is:
  // fas-icon(class="fa", icon="graduation-cap")
  // Vue.component('fas-icon', FontAwesomeIcon)
  Vue.component('FontAwesomeIcon', FontAwesomeIcon)
  // use <font-awesome-icon icon="user-clock" />
}
