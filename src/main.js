import './installCompositionApi.js'
import Vue from 'vue'
import App from './App.vue'
import icons from './icons' // font awesome
import router from './router'
import Vuetify from 'vuetify'
import '../node_modules/vuetify/dist/vuetify.min.css'
import '../node_modules/mdi/css/materialdesignicons.min.css'
import '../node_modules/material-design-icons/iconfont/material-icons.css'
import '../node_modules/font-awesome/css/font-awesome.min.css'
// https://materialdesignicons.com/

import 'vuetify/dist/vuetify.min.css'

Vue.filter('integerFormat', function (value) {
  if (!Number.isInteger(value)) return ''
  return Number(value).toLocaleString()
})

icons(Vue)
Vue.use(Vuetify, {} )

Vue.config.productionTip = false

new Vue({
  router,
  // vuetify : new Vuetify(),
  render: h => h(App)
}).$mount('#app')
