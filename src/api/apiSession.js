/*
This is a prototyping backend. It provides a "offline" demonstration
experience.

This also establishes a pattern to consider when it comes time to connect with the real backend.
 */

import { SYSTEM_TYPES, CvSessionMeta, CvSiteMeta } from '@/models'
import { prepareSiteMetaWithIoMeta } from '@/models/cvChannelMeta'


class ApiSession {
  constructor () {
    this.sessionsList = []

    const normalBlueSession = new CvSessionMeta({
      sessionName: 'Normal Blue Whale 2021-02-03',
      endDate: '2021-02-03T03:34:00',
      startDate: '2021-02-03T03:24:00',
      siteMeta: new CvSiteMeta({
        siteName: 'Normal Blue Boat',
        numberOfArrays: 2,
        stringsPerArray: 8,
        modulesPerString: 8,
        cellsPerModule: 24,
        systemType: SYSTEM_TYPES.BLUE,
      })
    })
    prepareSiteMetaWithIoMeta(normalBlueSession.siteMeta)
    Object.freeze(normalBlueSession)
    this.sessionsList.push(normalBlueSession)


    const minOrcaSession = new CvSessionMeta({
      sessionName: 'Tiny Orca 2021-02-03',
      endDate: '2021-02-03T03:34:00',
      startDate: '2021-02-03T03:24:00',
      siteMeta: new CvSiteMeta({
        siteName: 'Tiny Orca Boat',
        numberOfArrays: 1,
        stringsPerArray: 1,
        modulesPerString: 2,
        cellsPerModule: 2,
        systemType: SYSTEM_TYPES.ORCA,
      })
    })
    prepareSiteMetaWithIoMeta(minOrcaSession.siteMeta)
    this.sessionsList.push(minOrcaSession)


    console.log('normalBlueSession',this.sessionsList)
    // TODO a real back end would perform a lookup to find what sessions are available
  }

  /**
   * emulate a back end call to look up list of sessions that are available
   * @return {Promise<unknown>}
   */
  async apiFetchAvailableSessions () {
    const _this = this
    return new Promise(resolve => {
      setTimeout(function () {
        const names = _this.sessionsList.map(s => s.sessionName)
        resolve(names)
      }, 1000)
    })
  }

  /**
   * This emulates a backend that only loads a session on demand.
   * @param sessionName
   * @return {Promise<unknown>}
   */
  async apiFetchSessionMeta (sessionName) {
    const found = this.sessionsList.find( s => s.sessionName === sessionName )
    const time = found ? 1000 : 3000
    return new Promise((resolve, reject) => {
      setTimeout(function () {
        if ( !found ) return reject(`Session "${sessionName}" does not exist`)
        if ( found  ) return resolve(found)
        resolve(found)
      }, time)
    })
  }
}

// lazy construct and load
let apiSession
export function useSessionApi () {
  if (!apiSession) {
    apiSession = new ApiSession()
  }
  return {
    apiSession
  }
}
