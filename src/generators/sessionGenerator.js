import { ref } from '@vue/composition-api'
import { getChannelMeta } from '@/models/cvChannelMeta'
import { DEVICE_LEVELS, SYSTEM_TYPES } from '@/models'
import { CvSessionMeta } from '@/models'

const SAMPLE_SITE_META = {
  key:'sample_large_blue',
  systemType: SYSTEM_TYPES.BLUE,
  siteName: 'Sample Blue Whale 2021-01-30',
  cellsPerModule: 12,
  modulesPerString: 21,
  stringsPerArray: 6,
  numberOfArrays: 2,
  intervalInSeconds: 5,
  endDate: '2021-01-30T04:24:00',
  startDate: '2021-01-30T03:24:00'
}

const MINIMAL_SITE_META = {
  key:'sample_minimal_blue',
  systemType: SYSTEM_TYPES.BLUE,
  siteName: 'Minimal Blue Whale 2021-02-03',
  cellsPerModule: 2,
  modulesPerString: 2,
  stringsPerArray: 1,
  numberOfArrays: 1,
  intervalInSeconds: 30,
  endDate: '2021-02-03T03:34:00',
  startDate: '2021-02-03T03:24:00'
}

const MINIMAL_SITE_ORCA_META = Object.assign({} , MINIMAL_SITE_META)

MINIMAL_SITE_ORCA_META.key = 'sample_minimal_orca'
MINIMAL_SITE_ORCA_META.siteName = 'Minimal Orca 2021-02-03'
MINIMAL_SITE_ORCA_META.systemType = SYSTEM_TYPES.ORCA

function prepareSiteMetaWithIoMeta (siteMeta) {
  const channelMeta = getChannelMeta()
  siteMeta.stringMeta = {}
  siteMeta.moduleMeta = {}
  siteMeta.cellMeta = {}
  for (const [key, value] of Object.entries(channelMeta)) {
    switch (value.deviceLevel) {
    case DEVICE_LEVELS.CELL.name:
      siteMeta.cellMeta[key] = value
      break
    case DEVICE_LEVELS.MODULE.name:
      siteMeta.moduleMeta[key] = value
      break
    case DEVICE_LEVELS.STRING.name:
      siteMeta.stringMeta[key] = value
      break
    default:
      console.error('Could not determine device level of an IO Point Metadata object', key, value)
    }
  }
}

class SessionGener {
  constructor () {
    this.sessionList = ref([])
    this.load()
  }

  load () {
    let asStored = localStorage.getItem('sessionList') || '[]'
    this.sessionList.value = JSON.parse(asStored)
    // console.log('loaded session list', this.sessionList.value)
  }

  store() {
    localStorage.setItem('sessionList', JSON.stringify(this.sessionList.value))
    this.load()
  }

  findSession (sessionName) {
    let f = this.sessionList.value.find(e => e.sessionName === sessionName)
    console.log('findSite', sessionName, f)
    return f
  }

  addSessionToList (sessionName, siteMeta) {
    let found = this.findSession(sessionName)
    if (found) throw new Error('Not allowed to add same session name twice')
    let def = {sessionName: sessionName, siteMeta: siteMeta }
    console.log('re sessionDef', def, sessionName, siteMeta)
    let session = new CvSessionMeta(def)
    console.log('re session', session)
    this.sessionList.value.push(session)
    this.store()
  }

  removeOne(key) {
    this.sessionList.value = this.sessionList.value.filter( e => {
      return e.sessionName != key
    })
    this.store()
  }

  clearAllSessions () {
    localStorage.removeItem('sessionList')
    this.sessionList.value = []
  }
}
// export const useSessionGenerator = function () {
//   if (!init) initialize()
//   return {
//     findSession,
//     sessionList,
//     addSessionToList,
//     clearAllSessions
//   }
// }

let
  init = false

function initialize () {
  prepareSiteMetaWithIoMeta(MINIMAL_SITE_META)
  prepareSiteMetaWithIoMeta(SAMPLE_SITE_META)
  prepareSiteMetaWithIoMeta(MINIMAL_SITE_ORCA_META)
  init = true
}

export const useSessionGeneratorProto = function () {
  if (!init) initialize()
  return {
    MINIMAL_SITE_META, SAMPLE_SITE_META, MINIMAL_SITE_ORCA_META
  }
}

const sessionGenerator = new SessionGener()
export const useSessionGenerator = function () {
  if (!init) initialize()
  return {
    MINIMAL_SITE_META, SAMPLE_SITE_META, MINIMAL_SITE_ORCA_META,
    sessionGenerator
  }
}
