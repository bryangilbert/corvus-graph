
import { ref } from '@vue/composition-api'
import { CvSite } from '@/models'
import { prepareSiteMetaWithIoMeta } from '@/models/cvChannelMeta'


const SystemTypes = {
  Orca: {
    key: 'Orca',
    label: 'Orca',
    siteDef: {
      numberOfArrays: 1,
      stringsPerArray: 2,
      modulesPerString: 4,
      cellsPerModule: 12
    },
    bounds: {
      arrays: [1, 1],
      strings: [1, 4],
      modules: [1, 8],
      cells: [12, 12]
    }
  },
  Blue: {
    key: 'Blue',
    label: 'Blue',
    siteDef: {
      numberOfArrays: 2,
      stringsPerArray: 4,
      modulesPerString: 8,
      cellsPerModule: 24
    },
    bounds: {
      arrays: [1, 2],
      strings: [1, 8],
      modules: [1, 16],
      cells: [24, 24]
    }
  }
}

class SiteGener {
  constructor () {
    this.siteList = ref([])
    this.load()
  }

  load() {
    let asStored = localStorage.getItem('siteList') || '[]'
    this.siteList.value = JSON.parse(asStored)
    // console.log('loaded site list', this.siteList.value)
  }

  store() {
    const getCircularReplacer = () => {
      const seen = new WeakSet();
      return (key, value) => {
        if (typeof value === 'object' && value !== null) {
          if (seen.has(value)) {
            // console.log('detected circular reference on ', key, value)
            return;
          }
          seen.add(value);
        }
        return value;
      };
    };

    let jsonStr = JSON.stringify(this.siteList.value, getCircularReplacer());
    // console.log('storing site list', jsonStr)
    localStorage.setItem('siteList', jsonStr)
    this.load()
  }

  findSite(siteName) {
    return this.siteList.value.find( e => e.siteName === siteName)
  }

  connect(siteName) {
    this.disconnectAll()
    let site = this.findSite(siteName)
    site.isConnected = true
  }

  disconnect(siteName) {
    this.disconnectAll()
  }

  disconnectAll() {
    this.siteList.value.forEach( e => e.isConnected = false)
  }
  onlineSites() {
    return this.siteList.value.filter( e => e.isConnected )
  }

  addSiteToList (siteName, siteMeta) {
    let found = this.findSite(siteName)
    if (found) throw new Error('Not allowed to add same site name twice')
    let def = Object.assign({siteName: siteName}, siteMeta)
    def.siteName = siteName
    prepareSiteMetaWithIoMeta(def)
    // console.log('addSiteToList sitedef', siteName, 'siteMeta:', siteMeta, 'def:', def)
    let site = new CvSite(def)
    this.siteList.value.push(site)
    this.store()
  }

  removeOne(key) {
    this.siteList.value = this.siteList.value.filter( e => {
      return e.siteName != key
    })
    this.store()
  }

  clearAllSites() {
    localStorage.removeItem('siteList')
    this.siteList.value = []
  }
}

const siteGenerator = new SiteGener()
export const useSiteGenerator = function () {
  return {
    siteGenerator,
    SystemTypes,
  }
}
