import { SERIES_META } from '@/providers/seriesMeta'

export const useDataGenerator = function (d3) {
  return {
    generateDataForTimeSeries,
    generateTimeSeriesBetween,
    generateTimeSeries
  }
}

export const nextSinVal = function * nextVal( vmin, vmax, offset, invert) {
  const A = Math.round((vmax - vmin) / 2) // amplitude
  const B = .2 // period factor
  const C = offset // left offset
  const D = vmin + A // vetical shift to center series between vmin and vmax
  const pi = Math.PI
  let x = 0
  const J = function (x) {
    return 20 * Math.sin( (pi/180) * x)
  }
  let a = A
  while (true) {
    x++
    let x0 = B * (x + C)
    x0 = x0 * (pi / 180)
    let value = invert * a * Math.sin(x0) + D
    value += J(x)
    if (x % 5 === 0) {
      a += 0.5
      if(x % 100 === 0){
        a = A
      }
    }
    yield value
  }
}

export const nextStepVal = function * nextVal( vmin, vmax, offset, invert) {
  let x = 0
  const yStepper= randomIntFromInterval(1,5)
  const xStepper= randomIntFromInterval(60 * 4,60 * 6)
  const directionStepper = randomDirection()
  let direction = directionStepper.next().value
  let increment = yStepper.next().value
  let timeStep = xStepper.next().value
  let step = increment
  while (true) {
    x++
    if ( x % (timeStep) === 0) {
      step += increment * direction
      increment = yStepper.next().value
      timeStep = xStepper.next().value
      direction = directionStepper.next().value
    }
    direction = step >= vmax ? -1 : step <= vmin ? 1 : direction
    yield step
  }
}

function * nextTimeStep (startDate, intervalInSeconds = 30) {
  let nextDate = startDate
  while (true) {
    nextDate += intervalInSeconds * 1000
    yield nextDate
  }
}

function * randomIntFromInterval(min, max) { // min and max included
  while(true) {
    let v = Math.floor(Math.random() * (max - min + 1) + min)
    yield v
  }
}

function * randomDirection() {
  while(true) {
    let v = Math.random()
    v = v > 0.5 ? -1 : 1
    yield v
  }
}

function generateTimeSeries( hours ) {
  let dateNow = Date.now()
  let startDate = Math.round(dateNow - hours * 60 * 60 * 1000)
  return generateTimeSeriesBetween(startDate, dateNow)
}

function generateTimeSeriesBetween( startDate, endDate, intervalInSeconds ) {
  let nextDate = startDate
  let nextTimeStepper = nextTimeStep(startDate, intervalInSeconds)
  let timeSeries = []
  while (nextDate < endDate) {
    nextDate = nextTimeStepper.next().value
    timeSeries.push(nextDate)
  }
  return timeSeries
}

function generateDataForTimeSeries(index, timeSeries, channelDef) {
  let {type, min, max, offset } = channelDef
  let invert = channelDef.type === SERIES_META.CURRENT ? -1 : 1
  const dataSeries = {
    type: type,
    index: index,
    data: []
  }
  const nextV = type.generator(min, max, offset, invert)
  let cnt = 0
  timeSeries.forEach ( nextDate => {
    let value = nextV.next().value
    cnt++
    if (cnt < 300 || cnt > 510) {
      dataSeries.data.push({date: nextDate, value: value})
    } else {
      dataSeries.data.push({date: nextDate, value: false})
    }
  })
  return dataSeries
}
