import { appendPath } from '@/models'

export class CvChannel {
  constructor (container, key, channelMeta) {
    this.key = key
    this.title = channelMeta.title
    // unique id within a CvSite
    this.id = appendPath(container.path, key)
    this.parentPath = container.path
    this.path = this.id
    this.category = channelMeta.metaCategory.name
    this.units = channelMeta.metaCategory.units
    this.description = channelMeta.description
    this.deviceLevel = channelMeta.deviceLevel
    this.active = false
  }

  toggleActive() {
    console.log('CvChannel toggle active', this.key)
    this.active = !this.active
  }
}
