import { CvComponent, CvCell, DEVICE_LEVELS } from '@/models'

export class CvModule extends CvComponent{
  constructor (index, parentPath, site) {
    super(DEVICE_LEVELS.MODULE, index, parentPath, site)
    let limit = site.cellsPerModule
    for (let index = 0; index < limit; index++) {
      let child = new CvCell(index, this.path, site)
      this.children.push(child)
    }
  }
}
