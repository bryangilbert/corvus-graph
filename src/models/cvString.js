import { CvComponent, CvModule, DEVICE_LEVELS } from '@/models'


export class CvString extends CvComponent{
  constructor (index, parentPath, site) {
    super(DEVICE_LEVELS.STRING, index, parentPath, site)
    let limit = site.modulesPerString
    for (let index = 0; index < limit; index++) {
      let child = new CvModule(index, this.path, site)
      this.children.push(child)
      // addPoints(cvM, siteMeta.moduleMeta)
    }
  }
}
