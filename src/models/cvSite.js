import { CvArray, CvSiteMeta, makePath, splitPath } from '@/models'

export class CvSite extends CvSiteMeta {
  constructor (def) {
    super(def)
    // console.log('construct CvSite from def', def)
    const { key } = def
    this.key = key
    this.isConnected = false
    // this.isForPrototype = true
    this.children = []
    for(let index = 0; index < this.numberOfArrays; index++) {
      this.children.push(new CvArray(index, this))
    }
    this.componentPathList = this.buildComponentPathList()
  }

  /**
   * Given an array name (e.g. A02) search for this name in the site's array list
   * @param name
   * @return {*}
   */
  findCvArray(name) {
    return this.children.find((e) => {
      return e.name === name ? e : undefined
    })
  }

  /**
   * Search the component hierarchy  looking for component at the given path.
   * @param path e.g. A02/S06/M19/C08
   * @return {CvComponent}
   */
  getComponentFromPath(path) {
    let parts = splitPath(path)
    let name = parts.shift()
    let result = this.findCvArray(name)
    if (result && parts.length > 0) {
      let subpath = makePath(parts)
      result = result.getChildComponentFromPath(subpath)
    }
    // console.log('getComponentFromPath found: ', result)
    return result
  }

  getComponentPathList() {
    return this.componentPathList
  }

  getActivePaths() {
    return this.componentPathList.filter( e => e.active )
  }

  getActiveComponents() {
    let active = this.getActivePaths()
    return active.map( e => {
      return this.getComponentFromPath(e.path)
    })
  }

  getChannelsForComponents (componentList) {
    let all = []
    componentList.forEach( cp => {
      let cm = this.getComponentFromPath(cp.path)
      // console.log('getChannelsForComponents add cp', cp.path, cm.componentChannels)
      all = all.concat(cm.componentChannels)
    })
    return all
  }

  getIoChannelsForActiveComponents () {
    let ac = this.getActiveComponents()
    let all = []
    ac.forEach( c => {
      all = all.concat(c.componentChannels)
    })
    return all
  }

  getActiveIoChannelsForActiveComponents() {
    return this.getIoChannelsForActiveComponents().filter( e => e.active )
  }

  buildComponentPathList() {
    let paths = []
    this.children.forEach(cvA => {
      cvA.buildComponentPathList(paths)
    })
    paths = paths.sort( (ac,bc) => {
      let a = ac.path
      let b = bc.path
      if (a.length === b.length)
        return a - b
      return a.length - b.length
    })
    paths.forEach( p => p.active = false)
    return paths
  }
}
