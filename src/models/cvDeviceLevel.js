import { SYSTEM_TYPES } from '@/models'

class CvDeviceLevel {
  constructor (name) {
    this.name = name
  }

  getMeta(siteMeta) {
    let result = {}
    if (this.name === 'cv-cell') {
      result = siteMeta.cellMeta
    } else if (this.name === 'cv-module') {
      result = siteMeta.moduleMeta
    } else if (this.name === 'cv-string') {
      result = siteMeta.stringMeta
    }
    return result
  }

  getPrefix(systemType) {
    let result = ''
    if (this.name === 'cv-cell') {
      result = 'C'
    } else if (this.name === 'cv-module') {
      result = 'M'
    } else if (this.name === 'cv-string') {
      result = systemType === SYSTEM_TYPES.ORCA ? 'P' : 'S'
    } else if (this.name === 'cv-array') {
      result = 'A'
    }
    return result
  }

  getLabel(systemType) {
    let result = ''
    if (systemType === SYSTEM_TYPES.ORCA) {
      if (this.name === 'cv-cell') {
        result = 'Cell'
      } else if (this.name === 'cv-module') {
        result = 'Module'
      } else if (this.name === 'cv-string') {
        result = 'Pack'
      } else if (this.name === 'cv-array') {
        result = 'Array'
      }
    } else {
      if (this.name === 'cv-cell') {
        result = 'CvCell'
      } else if (this.name === 'cv-module') {
        result = 'CvModule'
      } else if (this.name === 'cv-string') {
        result = 'CvString'
      } else if (this.name === 'cv-array') {
        result = 'CvArray'
      }
    }
    return result
  }
}

export const DEVICE_LEVELS = {
  'CELL': (new CvDeviceLevel('cv-cell')),
  'MODULE': (new CvDeviceLevel('cv-module')),
  'STRING': (new CvDeviceLevel('cv-string')),
  'ARRAY': (new CvDeviceLevel('cv-array')),
}
