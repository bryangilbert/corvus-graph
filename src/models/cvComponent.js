import { getChannelMetaByKey } from '@/models/cvChannelMeta'
import { CvChannel, DEVICE_LEVELS } from '@/models'

export class CvComponent {
  constructor (deviceLevel, index, parentPath, site) {
    this.site = site
    this.children = []
    this.systemType = this.site.systemType
    this.index = index
    this.deviceLevel = deviceLevel
    this.typeStr = deviceLevel.getPrefix(this.systemType)
    this.name = this.typeStr + (String(index + 1).padStart(2,'0'))
    if (parentPath) {
      this.path = appendPath(parentPath, this.name)
    } else {
      this.path = this.name
    }
    // create the "IoPoint"/"Channel" paths and place into component
    let deviceLevelMeta = this.deviceLevel.getMeta(this.site)
    // console.log('deviceLevelMeta',this.name, site, deviceLevelMeta)
    let channelsByType = this._channelsByType(deviceLevelMeta)
    this.componentChannels = []
    channelsByType.forEach( typePointList => {
      let type = typePointList.typeName
      typePointList.cList.forEach( iop => this.componentChannels.push( iop ))
    })
  }

  _channelsByType (deviceLevelMeta) {
    // gather information by type
    let channelsByType = []
    for (const [key, value] of Object.entries(deviceLevelMeta)) {
      if (!value.metaCategory || !value.metaCategory.name) {
        console.error('missing value.metaCategory.name', value)
        continue
      }
      let typeName = value.metaCategory.name

      // get the list for the current type. Create empty list if none exists
      let typeChannelList = channelsByType.find(element => element.typeName === typeName)
      if (!typeChannelList) {
        typeChannelList = {typeName: typeName, cList: []}
        channelsByType.push(typeChannelList)
      }
      // stash the current key into the current type's list
      let channelMeta = getChannelMetaByKey(key)
      let cvChannel = new CvChannel(this, key, channelMeta)
      typeChannelList.cList.push(cvChannel)
    } // done collecting all the io points by type
    return channelsByType
  }

  getIoPaths() {
    return this.componentChannels
  }

  getAllIoPaths() {
    let all = []
    this.componentChannels.forEach( p => all.push(p))
    this.children.forEach( c => {
      c.getAllIoPaths().forEach(p => all.push(p))
    })
    return all
  }
  buildComponentPathList(paths) {
    paths.push({'path': this.path, typeStr: this.typeStr} )
    this.children.forEach( c => c.buildComponentPathList(paths))
  }

  /**
   * Search the component hierarchy  looking for component at the given path.
   * @param path e.g. A02/S06/M19/C08
   * @return {CvComponent}
   */
  getChildComponentFromPath(path) {
    let result = this
    let parts = splitPath(path)
    // get the first element of the path and remove from list of parts
    let name = parts.shift()
    while (name) {
      // does this device level have children (e.g. not a CvCell)
      if (result.children.length > 0 ) {
        // look for a child with the given name
        let child = result.children.find((e) => { return e.name === name})
        if (child) {
          // found one so prepare to step down a level
          result = child
          name = parts.shift()
        } else {
          name = undefined // end while loop. This level has no children with the given name
        }
      } else {
        name = undefined // end while loop. this level has no children
      }
    }
    return result
  }

}

export const makePath = function makePath( parts) {
  return parts.join('/')
}

export const splitPath = function splitPath( path) {
  return path.split('/')
}

export const appendPath = function appendPath( path, newPart) {
  let parts = splitPath(path)
  return makePath( [...parts, newPart ])
}

