import { CvSite, CvSessionMeta } from '@/models'

export class CvSession extends CvSessionMeta {
  constructor (def) {
    super(def)
    this.cvSite = new CvSite(this.siteMeta)
  }
  getSite () {
    return this.cvSite
  }
  getSiteName() {
    return this.cvSite.siteName
  }
}
