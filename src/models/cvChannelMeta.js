import { DEVICE_LEVELS } from '@/models'

const metaCategories = {
  'ALARMS' : { name: 'alarms', units: ''},
  'CURRENT' : { name: 'current', units: 'A'},
  'MISCELLANEOUS' : { name: 'misc', units: ''},
  'RESISTANCE' : { name: 'resistance', units: 'Ohm'},
  'SOC' : { name: 'soc', units: '%'},
  'SOH' : { name: 'soh', units: '%'},
  'STATUS' : { name: 'status', units: ''},
  'TEMPERATURE' : { name: 'temperature', units: 'C'},
  'VOLTAGE' : { name: 'voltage', units: 'V'},
}

let lazyData
function getLazy( key) {
  if (!lazyData) {
    lazyData = generateData()
  }
  return lazyData
}

export const getChannelMeta = function ( key) {
  return getLazy()
}

export const getChannelMetaByKey = function ( key) {
  return getLazy()[key]
}

export const prepareSiteMetaWithIoMeta = function (siteMeta) {
  siteMeta.stringMeta = {}
  siteMeta.moduleMeta = {}
  siteMeta.cellMeta = {}
  const channelMeta = getLazy()
  for (const [key, value] of Object.entries(channelMeta)) {
    switch (value.deviceLevel) {
    case DEVICE_LEVELS.CELL.name:
      siteMeta.cellMeta[key] = value
      break
    case DEVICE_LEVELS.MODULE.name:
      siteMeta.moduleMeta[key] = value
      break
    case DEVICE_LEVELS.STRING.name:
      siteMeta.stringMeta[key] = value
      break
    default:
      console.error('Could not determine device level of an IO Point Metadata object', key, value)
    }
  }
}

// See shared/api/iopoints.js for much of this inspiration
// and note need to rationalize the following with that api
// and note that the following does not have most of the pack (string) level points
function generateData() {
  const CELL_D = DEVICE_LEVELS.CELL.name
  const MOD_D = DEVICE_LEVELS.MODULE.name
  const STR_D = DEVICE_LEVELS.STRING.name
  return {
    CVOLT: {
      deviceLevel: CELL_D,
      name: 'CVOLT',
      title: 'Cell Voltage',
      description: 'Cell Voltage',
      metaCategory: metaCategories.VOLTAGE
    },
    CTEMP: {
      deviceLevel: CELL_D,
      name: 'CTEMP',
      title: 'Cell Temperature',
      description: 'Cell Temperature',
      metaCategory: metaCategories.TEMPERATURE
    },
    CSOC: {
      deviceLevel: CELL_D,
      name: 'CSOC',
      title: 'Cell SOC',
      description: 'Cell SOC',
      metaCategory: metaCategories.SOC
    },
    CSOH: {
      deviceLevel: CELL_D,
      name: 'CSOH',
      title: 'Cell SOH',
      description: 'Cell SOH',
      metaCategory: metaCategories.SOH
    },
    CRESSTD: {
      deviceLevel: CELL_D,
      name: 'CRESSTD',
      title: 'Cell Resistance',
      description: 'Cell Resistance',
      metaCategory: metaCategories.RESISTANCE
    },
    MASEOVERVOLT: {
      deviceLevel: MOD_D,
      name: 'MASEOVERVOLT',
      title: 'Module SE Overvoltage Alarm',
      description: 'Module Series Element Over Voltage Alarm.',
      metaCategory: metaCategories.ALARMS
    },
    MASEUNDERVOLT: {
      deviceLevel: MOD_D,
      name: 'MASEUNDERVOLT',
      title: 'Module SE Undervoltage Alarm',
      description: 'Module Series Element Undervoltage Alarm.',
      metaCategory: metaCategories.ALARMS
    },
    MVOLTAGE: {
      deviceLevel: MOD_D,
      name: 'MVOLTAGE',
      title: 'Module Voltage',
      description: 'Module Voltage.',
      metaCategory: metaCategories.VOLTAGE
    },
    MTEMPAMB: {
      deviceLevel: MOD_D,
      name: 'MTEMPAMB',
      title: 'Module Ambient Temperature',
      description: 'Module Ambient Temperature',
      metaCategory: metaCategories.TEMPERATURE
    },
    MSOCAVG: {
      deviceLevel: MOD_D,
      name: 'MSOCAVG',
      title: 'Module SE State of Charge Avg',
      description: 'Module SE State of Charge Avg',
      metaCategory: metaCategories.SOC
    },
    MSOHAVG: {
      deviceLevel: MOD_D,
      name: 'MSOHAVG',
      title: 'Module SE State of Health Avg',
      description: 'Module SE State of Health Avg',
      metaCategory: metaCategories.SOH
    },
    MRESSTDAVG: {
      deviceLevel: MOD_D,
      name: 'MRESSTDAVG',
      title: 'Avg Resistance',
      description: 'Module Avg Resistance',
      metaCategory: metaCategories.RESISTANCE
    },
    MTEMPCON: {
      deviceLevel: MOD_D,
      name: 'MRESLIVMIN',
      title: 'Module Connector Temperature',
      description: 'Module Connector temperature.',
      metaCategory: metaCategories.TEMPERATURE
    },
    PASEOVERTEMP: {
      deviceLevel: STR_D,
      name: 'PASEOVERTEMP',
      title: 'Pack SE Overtemperature Alarm',
      description: 'Pack Series Element Overtemperature Alarm.',
      metaCategory: metaCategories.ALARMS
    },
    PVBATT: {
      deviceLevel: STR_D,
      name: 'PVBATT',
      title: 'Pack Battery Voltage',
      description: 'Pack Battery Voltage.',
      metaCategory: metaCategories.VOLTAGE
    },
    PVBUS: {
      deviceLevel: STR_D,
      name: 'PVBUS',
      title: 'Pack Load Voltage',
      description: 'Pack Load Voltage.',
      metaCategory: metaCategories.VOLTAGE
    },
    PCURRENT: {
      deviceLevel: STR_D,
      name: 'PCURRENT',
      title: 'Pack Current',
      description: 'Pack Current.',
      metaCategory: metaCategories.CURRENT
    },
    PSEVAVG: {
      deviceLevel: STR_D,
      name: 'PSEVAVG',
      title: 'Pack SE Voltage Avg',
      description: 'Pack SE Voltage Avg.',
      metaCategory: metaCategories.VOLTAGE
    },
  }
}
