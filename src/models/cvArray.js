import { CvComponent, CvString, DEVICE_LEVELS } from '@/models'

export class CvArray extends CvComponent {
  constructor (index, site) {
    super(DEVICE_LEVELS.ARRAY, index, undefined, site)
    let limit = site.stringsPerArray
    for (let index = 0; index < limit; index++) {
      let child = new CvString(index, this.path, site)
      this.children.push(child)
    }
  }

}
