export class CvSessionMeta {
  /**
   * siteMeta is to be an instance of CvSiteMeta
   * @param def
   */
  constructor ( def ) {
    const { endDate, sessionName, siteMeta, startDate } = def
    this.endDate = endDate
    this.sessionName = sessionName
    this.siteMeta = siteMeta
    this.startDate = startDate
  }
}
