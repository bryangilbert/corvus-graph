export class CvSiteMeta {
  constructor ( def ) {
    const {siteName, numberOfArrays, systemType,
      stringsPerArray, stringMeta,
      modulesPerString, moduleMeta,
      cellsPerModule, cellMeta
    } = def
    this.siteName = siteName
    this.systemType = systemType
    this.numberOfArrays = numberOfArrays
    this.stringsPerArray = stringsPerArray
    this.stringMeta = stringMeta
    this.modulesPerString = modulesPerString
    this.moduleMeta = moduleMeta
    this.cellsPerModule = cellsPerModule
    this.cellMeta = cellMeta
  }
}
