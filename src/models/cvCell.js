import { CvComponent, DEVICE_LEVELS } from '@/models'

export class CvCell extends CvComponent{
  constructor (index, parentPath, site) {
    super(DEVICE_LEVELS.CELL, index, parentPath, site)
  }
}
