import Vue from 'vue'
import VueRouter from 'vue-router'
import OfflineControlsView from '@/offline/OfflineControlsView.vue'
import Session from '@/views/Session.vue'
import System from '@/views/System.vue'
import SystemData from '@/views/SystemData.vue'
import SessionSelector from '@/views/SessionSelector.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: SessionSelector
  },
  {
    path: '/offline',
    name: 'Offline',
    component: OfflineControlsView
  },
  {
    path: '/system',
    name: 'System',
    component: System
  },
  {
    path: '/sysData',
    name: 'SystemData',
    component: SystemData
  },
  {
    path: '/sessions',
    name: 'Sessions',
    component: SessionSelector
  },
  {
    path: '/session/:sessionName',
    name: 'Session',
    component: Session
  }
]

const router = new VueRouter({
  routes
})

export default router
