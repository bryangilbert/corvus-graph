# Some notes for ...

  - SOH <70%
  - Cell temperature > 50C
  - Cell voltage < 2700 mv or > 3400 mV
  - Modules have 24 cells in BW  12 in Orca

  - 14 modules max in a string
  - max 64 strings per "array" which is probably customer array bus and not physical pack array

  - can limit the # of channels in the graph to something reasonable to viw. E.g. 30

## calcs

```
	Min	max	avg		Ahr	
cell	2.7	3.4	3.05		40	guess
						
						
# in Orca module	12					
# in BW module	24					
						
Orca module	32.4	40.8	36.6		480	
BW module	64.8	81.6	73.2		960	
						
						
# modules in string BW	14					
String Voltage	907.2	1142.4	1024.8		13440	
						
						
Typical load	2000	A			6.72	Hours at load
```
