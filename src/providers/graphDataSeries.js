
import { useDataGenerator, nextSinVal, nextStepVal} from '@/generators/dataGenerator'
import { getDims } from '@/components/graphHelper'
import { SERIES_META } from '@/providers/seriesMeta'

SERIES_META.VOLTAGE.generator = nextSinVal
SERIES_META.CURRENT.generator = nextSinVal
SERIES_META.TEMPERATURE.generator= nextStepVal

export const useSystemHelper = function () {
  return {
    createSystem
  }
}
const dataHelper = useDataGenerator()
const HOURS = 24

function addSeries(dims, allSeries, someSeries) {
  // todo fix this hack .. find a better way than to modify the global dims
  // dims.left += -1 * someSeries.yAxisOffset
  // someSeries.index++
  allSeries.push(someSeries)
}

function createSeries(defn, index, lineColour, yAxisOffset, timeSeries) {
  const series =  {
    type: defn.type,
    name: defn.name,
    index: index,
    count: 0,
    lineColour: lineColour,
    units: 'A',
    yAxisOffset: index * yAxisOffset,
    minValue: Number.MAX_SAFE_INTEGER, // high so the generated series pull it down
    maxValue: Number.MIN_SAFE_INTEGER, // low so the generated series pull it high
    // dates are numbers - milliseconds
    minDate: Number.MAX_SAFE_INTEGER, // big so generated series can pull it down
    maxDate: Number.MIN_SAFE_INTEGER,
    timeSeries: timeSeries,
    series: [
    ]
  }
  if (timeSeries.length > 0 ) {
    series.minDate = timeSeries[0]
    series.maxDate = timeSeries[timeSeries.length - 1]
  } else {
    series.minDate = Number.MAX_SAFE_INTEGER
    series.maxDate = Number.MIN_SAFE_INTEGER
  }
  return series
}

const arrayMin = (arr, start) => {
  let m = start
  arr.forEach( (elem) => m = Math.min(m,elem.value))
  return m
}
const arrayMax = (arr, start) => {
  let m = start
  arr.forEach( (elem) => m = Math.max(m,elem.value))
  return m
}

function createSystem (channels) {
  let system = {}
  system.dims = getDims()
  system.timeSeries = dataHelper.generateTimeSeries( HOURS)
  system.allSeries = []
  system.cumulativeOffset = 0
  system.seriesCnt = 0
  channels.forEach((s) => {
    if (s.activeInGraph) {
      // console.log('createSystem with a s', s)
      let container = system.allSeries.find(c => c.type === s.type)
      if (!container) {
        let clr = s.type.lineColour
        system.cumulativeOffset += system.dims.yAxisOffset
        container = createSeries(s, system.seriesCnt, clr, system.cumulativeOffset, system.timeSeries)
        system.allSeries.push(container)
        system.seriesCnt++
      }
      container.count++
      let dataSeries = dataHelper.generateDataForTimeSeries(container.count, system.timeSeries, s)
      let min = arrayMin(dataSeries.data, container.minValue)
      let max = arrayMax(dataSeries.data, container.maxValue)
      container.series.push(dataSeries)
      container.minValue = Math.min(container.minValue, min)
      container.maxValue = Math.max(container.maxValue,  max)

    }
  })
  return system
}
