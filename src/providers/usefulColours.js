export const usefulColours = [
  'darkslategrey',
  'tomato',
  'chocolate',
  'darkred',
  'darkmagenta',
  'blue',
  'slateblue',
  'navy',
  'midnightblue',
  'steelblue',
  'darkslateblue',
  'darkcyan',
  'teal',
  'green',
  'darkgreen',
  'forestgreen'
]
