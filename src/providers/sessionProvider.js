/*
This module provides access to sessions and is a middleware between the backend (prototype or real) and the UI.
This module implements the business logic for the UI so the UI modules can focus on being UI modules.
 */
import { useSessionApi } from '@/api/apiSession'
import { CvSession } from '@/models'
import { useUiSystem } from '@/providers/uiSystem'
const { loadStart, loadFinish } = useUiSystem()
const { apiSession } = useSessionApi()

const WAITING_KEY_OPEN = 'openSession'

class SessionProvider {
  constructor () {
    this.waitingLists = {}
    this.waitingLists[WAITING_KEY_OPEN] = []
    this.sessionList = []
  }

  /**
   * returns an array of session names
   * @return {Promise<unknown>}
   */
  async fetchSessionList () {
    loadStart('listSessions') // start a progress indicator
    return new Promise(async (resolve) => {
      let sessions = await apiSession.apiFetchAvailableSessions()
      loadFinish('listSessions')
      resolve(sessions)
    })
  }

  /**
   * Fetches and returns the requested session.
   * Rejects if sessionName is not available
   * @param sessionName
   * @return {Promise<unknown>}
   */
  async openSession (sessionName) {
    const session = this.findSession(sessionName)
    if (session) {
      console.log('openSession found', session)
      return Promise.resolve(session)
    }
    // Many UI components may invoke this open in nearly the same time.
    // Certainly within the 3 seconds the prototype backend delays.
    // Aggregate the callers into a waiting list and give them all the same response.
    // This approach can be extended to other popular long-running api calls
    const waitingList = this.waitingLists[WAITING_KEY_OPEN]
    const _this = this
    const listName = `${WAITING_KEY_OPEN}(${sessionName})`
    // console.log(listName, ' not found so need to make api call')
    return new Promise((resolveForCaller) => {
      loadStart(listName) // start a progress indicator
      // console.log('park callers in the waiting list pending the first caller\'s response')
      waitingList.push((result) => {
        // console.log('process the result for each caller', result)
        loadFinish(listName)
        resolveForCaller(result)
      })
      // console.log('Send the API request for just the first caller')
      if (waitingList.length === 1) {
        apiSession.apiFetchSessionMeta(sessionName)
          .then((apiResult) => {
            const session = new CvSession(apiResult)
            this.sessionList.push(session)
            // console.log('resolve each of the callers waiting for the long running result')
            waitingList.forEach(cb => cb(session))
            // console.log('clear the waiting list')
            _this.waitingLists[WAITING_KEY_OPEN] = []
          })
      }
    })
  }

  findSession (sessionName) {
    return this.sessionList.find((s) => s.sessionName === sessionName )
  }

  /**
   * Release the memory used by a session when it is no longer needed.
   * Thinking that sessions may be displayed in a tab layout where each
   * tab can be closed when not wanted.
   * @param name
   * @return {boolean}
   */
  closeSession (sessionName) {
    let success = false
    let index = this.sessionList.findIndex((s) => s.sessionName === sessionName)
    if (index >= 0) {
      this.sessionList.splice(index, 1)
    }
    return success
  }
}

// lazy construct
let sessionProvider
export function useSessionProvider () {
  if (!sessionProvider) sessionProvider = new SessionProvider()
  return {
    sessionProvider
  }
}
