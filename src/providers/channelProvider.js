import { ref, computed } from '@vue/composition-api'
import { useSessionProvider } from '@/providers/sessionProvider'
const { sessionProvider } = useSessionProvider()

const channels = ref([])

class ChannelProvider {
  constructor () {
    this.channels = []
  }

  async _getSite ( sessionName ) {
    const session = await sessionProvider.openSession(sessionName)
    return session.getSite()
  }

  async getAllChannels ( sessionName ) {
    let site = await this._getSite(sessionName)
    let activeChannels =  site.getIoChannelsForActiveComponents()
    console.log('do we have a activeChannels here?', activeChannels)
    return activeChannels
  }

  async getChannelsForActiveComponents ( sessionName ) {
    let site = await this._getSite(sessionName)
    return site.getIoChannelsForActiveComponents()
  }
}

// lazy construct and load
let channelProvider
export function useChannelProvider () {
  if (!channelProvider) channelProvider = new ChannelProvider()
  return {
    channelProvider
  }
}
