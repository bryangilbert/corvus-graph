import { ref, computed } from '@vue/composition-api'
import { SERIES_META } from '@/providers/seriesMeta'

const channels = ref([])
const defaults = [
  {
    name: 'p0/m0/voltage',
    type: SERIES_META.VOLTAGE,
    min: 700, max: 800, offset: 200,
    activeInGraph: true
  },
  {
    name: 'p0/m1/voltage',
    type: SERIES_META.VOLTAGE,
    min: 650, max: 785, offset: -100,
    activeInGraph: true
  },
  {
    name: 'p0/m0/current',
    type: SERIES_META.CURRENT,
    min: -20, max: 100, offset: 50,
    activeInGraph: true
  },
  {
    name: 'p0/m1/current',
    type: SERIES_META.CURRENT,
    min: -24, max: 80, offset: -110,
    activeInGraph: true
  },
  {
    name: 'p0/m0/temperature',
    type: SERIES_META.TEMPERATURE,
    min: 30, max: 40, offset: -110,
    activeInGraph: true
  }
]
defaults.forEach((ch => channels.value.push(ch)))

export function useChannelProvider () {
  const activeChannels = computed ( () => channels.value.filter( (c) => c.activeInGraph))
  return {
    channels: channels,
    activeChannels
  }
}

export default function useProduct() {
  const loading = ref(false)
  const products = ref([])

  async function search (params) {
    loading.value = true
    products.value = await fetchProduct(params)
    loading.value = false
  }
  return {
    loading: computed(() => loading.value),
    products: computed(() => products.value),
    search
  }
}
