
import { usefulColours } from '@/providers/usefulColours'

export const SERIES_META = {
  VOLTAGE: {
    label: 'voltage',
    lineColour: usefulColours[0],
    units: 'V'
  },
  CURRENT: {
    label: 'current',
    lineColour: usefulColours[3],
    units: 'A'
  },
  TEMPERATURE: {
    label: 'temperature',
    lineColour: usefulColours[6],
    units: 'C'
  }
}

