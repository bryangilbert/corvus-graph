import { ref, computed } from '@vue/composition-api'

let loadingCount = ref(0)

export function useUiSystem () {
  const loadStart = (optionalName) => {
    loadingCount.value++
    // console.log('start', optionalName, loadingCount.value, loadingActive.value)
  }
  const loadFinish = (optionalName) => {
    loadingCount.value--
    // console.log('finish', optionalName, loadingCount.value, loadingActive.value)
  }
  const loadingActive = computed(() => loadingCount.value > 0)
  return {
    loadingActive,
    loadStart,
    loadFinish
  }
}
