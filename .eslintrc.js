module.exports = {
  extends: [
    // 'eslint:recommended',
    'plugin:vue/recommended'
  ],
  root: true,
  env: {
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'semi': 'off',
    'indent': ['error', 2],
    'quotes': [
      'error',
      'single',
      {
        'avoidEscape': true
      }
    ],
    'vue/max-attributes-per-line': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
  }
}
